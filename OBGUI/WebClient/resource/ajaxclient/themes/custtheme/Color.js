qx.Theme.define("qx.theme.custtheme.Color", {
	extend : ajaxclient.theme.modern.Color,
	colors : {
		
		"buttongradiant-border" : "#FC8000",
		"buttongradiant-border-focus" : "#B0E0E0",
		"buttongradiant-label" : "#FFFFFF",
		"buttongradiant-label-hover" : "#3E3391"
	}
});