qx.Theme.define("qx.theme.ls.Decoration", {
    extend: ajaxclient.theme.simple.Decoration,

    aliases: {
        decoration: "qx/decoration/Simple"
    },

    decorations: {

        "textfield": {
            include: "inset",
            style: {
                width: 5,
                innerWidth: 5,
                color: "#DDAAFF",
                colorBottom: "#00DDFF",
                backgroundColor: "#0000FF"
            }
        },

        "textfield-focused": {
            include: "textfield",
            style: {
                backgroundColor: "#DA0DFF"
            }
        },

        "custompassword": {
            style: {
                backgroundColor: "#DAD0FF",
                color: "#DDAAFF",
                colorBottom: "#00DDFF",
                width: 10
            }
        },

        "custompassword-focused": {
            style: {
                backgroundColor: "#DAFFD0",
                color: "#00DDFF",
                colorBottom: "#DDAAFF",
                width: 10
            }
        },

        "customimage": {
            style: {
                backgroundColor: "black",
                color: "red",
                width: 1
            }
        },

        "customspinner": {
            style: {
                backgroundColor: "yellow",
                color: "blue",
                width: 1
            }
        },

        "spinnertextfield": {
            style: {
                backgroundColor: "yellow"
            }
        },

        "spinnertextfield-focused": {
            include: "spinnertextfield",
            style: {
                backgroundColor: "black",
                colorBottom: "red",
                width: 2
            }
        },

        "customcontainer": {
            style: {
                color: "blue",
                innerColor: "yellow",
                backgroundColor: "#FAFA90",
                width: 5,
                widthTop: 10,
                innerWidth: 10,
                radius: 15,
                style: "dotted",
                styleTop: "solid"

            }
        },

        "customtree": {
            style: {
                width: [5, 10],
                color: "black",
                style: "double",
                backgroundColor: "maroon"
            }
        },

        "customgroupbox": {
            style: {
                width: 4,
                color: "black",
                backgroundColor: "#400000"
            }
        },

        "groupframe": {
            style: {
                width: 1,
                style: "dashed",
                radius: 5,
                startColor: "#700000",
                endColor: "#F00000"
            }
        },

        "tabviewpane": {
            style: {
                startColor: "#FFFFFF",
                startColorPosition: 10,
                radius: 5,
                orientation: "horizontal",
                endColor: "#F00000"
            }
        },

        "customcombobox": {
            style: {
                startColor: "#003DF5",
                radiusBottomRight: 10,
                orientation: "horizontal",
                endColor: "#002EB8"
            }
        },

        "comboboxpane": {
            style: {
                width: 5,
                color: "red",
                backgroundColor: "#33CCFF"
            }
        },

        "combobutton": {
            style: {
                width: 1,
                color: "black",
                radius: 3,
                radiusBottomRight: 10,
                startColor: "#CC33FF",
                endColor: "#FF33CC"
            }
        },

        "combobutton-hovered": {
            include: "combobutton",
            style: {
                color: "white"
            }
        },

        "combobutton-pressed": {
            include: "combobutton-hovered",
            style: {
                startColor: "#FF33CC",
                endColor: "#CC33FF"
            }
        },

        "customdatepicker": {
            style: {
                backgroundColor: "#FF6633"
            }
        },

        "customdatepickertext": {
            style: {
                backgroundColor: "#F5B800",
                color: "blue",
                widthBottom: 3
            }
        },

        "customprogressbar": {
            style: {
                width: 1,
                color: "#6633FF",
                backgroundColor: "#CCFF33"
            }
        },

        "custombutton": {
            style: {
                width: 5,
                innerWidth: 2,
                innerColor: "#33FFCC",
                color: ["#33FF66", "#66FF33", "#33FF66", "#66FF33"],
                startColor: "#6633FF",
                endColor: "#CC33FF"
            }
        },
        "custombutton-hovered": {
            include: "custombutton",
            style: {
                style: "dotted",
                innerColor: "#FF33CC"
            }
        },
        "custombutton-pressed": {
            include: "custombutton-hovered",
            style: {
                startColor: "#CC33FF",
                endColor: "#6633FF"
            }
        },

        "customtoolbarmenu": {
            style: {
                color: "blue",
                width: 1,
                radius: 5,
                startColor: "#CC33FF",
                endColor: "white"
            }
        },

        "customsplitbutton": {
            style: {
                color: "fuchsia",
                width: 2,
                radius: 5,
                startColor: "blue",
                endColor: "#CC33FF"
            }
        },

        "customtoolbarseparator": {
            style: {
                widthLeft: 2,
                colorLeft: "rgba(99, 99, 99, 0.5)"
            }
        },

        "customtoolbar": {
            style: {
                backgroundColor: "#D00000"
            }
        },

        "defaultscrollarea": {
            style: {
                width: 1,
                style: "solid",
                color: "gray"
            }
        }
    }
});
