﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="java ns"     xmlns="http://www.seagullsoftware.com/schemas/legasuite/panel"
    xmlns:ns="http://www.seagullsoftware.com/schemas/legasuite/panel"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:java="http://xml.apache.org/xslt/java">
<!-- CHANGE SEQUENCE (Grid column)                      -->
<!-- Search criteria:                                   -->
<!--   Find property Field ID with value "GRIDCOLUMN1". -->
<!-- Actions:                                           -->
<!--   Set property Field ID to #ROPTX.                 -->
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" standalone="yes" />
<xsl:template match="ns:ajax_grid_column">
  <xsl:choose>
      <xsl:when test="(./ns:id/text()[normalize-space() = normalize-space('GRIDCOLUMN1')])">
        <xsl:copy>
           <xsl:apply-templates select="*[not(self::ns:id)]"/>
					<id>
						<xsl:value-of select="./ns:host_field" />
					</id>
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
           <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
      </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Copy an ordinary node or attribute-->
<xsl:template match="@*|node()">
  <xsl:copy>
     <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>
</xsl:stylesheet>
