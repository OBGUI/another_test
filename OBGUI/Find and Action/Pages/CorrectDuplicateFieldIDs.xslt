﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="java ns"
	xmlns="http://www.seagullsoftware.com/schemas/legasuite/panel"
	xmlns:ns="http://www.seagullsoftware.com/schemas/legasuite/panel"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java">

	<xsl:output method="xml" version="1.0" encoding="UTF-8"
		indent="yes" standalone="yes" />

	<xsl:template match="ns:id">
		<xsl:variable name="id" select="normalize-space(text())" />
		<xsl:choose>
			<xsl:when
				test="count(preceding::ns:id[normalize-space(text()) = $id]) &gt; 0">
				<xsl:copy>
					<xsl:apply-templates select="*[not(self::ns:id)]" />
					<id>
						<xsl:value-of
							select="self::ns:host_field(text())" />
					</id>
				</xsl:copy>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@*|node()" />
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
